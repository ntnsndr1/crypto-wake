# Contributing

Anyone is welcome to contribute to Crypto Wake.

## General guidelines

* Employ a tone of celebration mixed with honest assessment
* Highlight lessons for future builders
* Err on the side of using past tense even if a project is nominally still in existence
* Ignore outright scam projects unless they furnish especially interesting lessons
* Projects may still be operating, but they should be meaningfully inactive for at least a year in terms of development, use, and market value
* Cite source material as much as possible with in-text hyperlinks


## Entry format

Each entry should have the following sections:

* Introduction - A brief summary of the project, around one paragraph
* Lore - A history of the project, including an explanation of its demise
* Lessons - Distill lessons from the project
* Links - Provide useful links about the project, starting with the official "Website," "Wikipedia" page, and "Codebase," if applicable. Use Wayback Machine links for inactive (or likely to become inactive) websites.
