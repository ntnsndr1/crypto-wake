# Crypto Wake

Currently hosted at [cryptowake.medlab.host](https://cryptowake.medlab.host).

## How to contribute

The Projects files are located in `files/docs/projects/`. See more details about how to contribute on the [Contributing page](https://cryptowake.medlab.host/about/contributing/).

## Roadmap

When ready to adjust URL, be sure to update:

- GitLab settings > CI/CD > Variables: `CLOUDRON_DOCS_SURFER_TOKEN`
- .gitlab-ci.yml: `DOCS_SERVER`
- files/mkdocs.yml: `site_url`

## Models

* [Civic Tech Graveyard](https://civictech.guide/graveyard/)

## Open questions

- Should this be migrated to Jeykll with a csv backend? Or a Google doc?
    - Would be easier to maintain the database and uniform layout
